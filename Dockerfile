#FROM node:18.12.1

#ENV DEBIAN_FRONTEND=noninteractive \
#    ANDROID_SDK=/opt/android-sdk-linux \
#    ANDROID_HOME=/opt/android-sdk-linux \
#    ANDROID_SDK_HOME=/opt/android-sdk-linux \
#    ANDROID_SDK_ROOT=/opt/android-sdk-linux

#RUN dpkg --add-architecture i386 \
#    && apt-get update -yqq \
#    && apt-get install -y \
#    curl \
#    expect \
#    git \
#    make \
#    libc6:i386 \
#    libgcc1:i386 \
#    libncurses5:i386 \
#    libstdc++6:i386 \
#    zlib1g:i386 \
#    libarchive-tools:i386 \
#    openjdk-11-jdk \
#    wget \
#    unzip \
#    vim \
#    openssh-client \
#    locales \
#    && apt-get clean \
#    && rm -rf /var/lib/apt/lists/* \
#    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

# ENV LANG=en_US.UTF-8

# RUN groupadd android && useradd -d /opt/android-sdk-linux -g android -u 999 android

# COPY tools /opt/tools

# COPY licenses /opt/licenses

# WORKDIR /opt/android-sdk-linux

# RUN /opt/tools/entrypoint.sh built-in

# CMD /opt/tools/entrypoint.sh built-in

FROM runmymind/docker-android-sdk:ubuntu-standalone
# git: https://github.com/mindrunner/docker-android-sdk/blob/master/ubuntu/standalone/Dockerfile

ENV NPM_VERSION=10.4.0 \
    NODE_VERSION=20.11.0 \
    NPM_CONFIG_CACHE=/home/cache \
    PATH=${PATH}:${ANDROID_HOME}/emulator:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:/opt/node/bin:/home/node/.npm-global/bin

RUN echo "=== Arquitetura I386 para instalar bibliotecas para o NodeJS ===" \
    && dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get install -y --no-install-recommends gnupg \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
    yarn \
    libc6:i386 \
    libncurses5:i386 \
    libstdc++6:i386 \
    lib32z1 \
    g++ \
    make \
    gcc \
    && echo "=== NodeJS ===" \
    && mkdir -p /home/node/.npm-global \
    && wget -q http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz \
    && tar -xzf node-v${NODE_VERSION}-linux-x64.tar.gz \
    && mv node-v${NODE_VERSION}-linux-x64 /opt/node \
    && rm node-v${NODE_VERSION}-linux-x64.tar.gz \
    && echo "=== NPM ===" \
    && npm i -g npm@${NPM_VERSION} \
    && chown -R 1000:1000 /home/cache \
    && echo "=== Apagando pacotes não utilizados ===" \
    && apt-get autoremove -y \
    && apt-get remove -y \
    && apt-get clean

WORKDIR /projeto