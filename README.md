# Docker - React Native

![Badge](https://img.shields.io/static/v1?label=nodejs&message=20.11.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=npm&message=10.4.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=yarn&message=1.22.19&color=blue)
![Badge](https://img.shields.io/static/v1?label=react&message=%5E18.2.0&color=blue)
![Badge](https://img.shields.io/static/v1?label=react-native&message=%5E0.73.4&color=blue)
[![Badge](https://img.shields.io/static/v1?label=bitbucket&message=ygo_morais&color=green&logo=bitbucket)](https://bitbucket.com/ygo_morais/docker-react-native)
![Badge](https://img.shields.io/static/v1?label=license&message=MIT&color=green)

## Sobre

Imagem para criação de containers para desenvolver aplicativos com React Native.

_Obs: Para visualizar o Dockerfile file atualizado, acesse <https://bitbucket.com/ygo_morais/docker-react-native>._

## Criando um projeto

```bash
docker container run -it --rm --net host -v $PWD:/projeto ygoamaral/react-native:latest npx react-native init <nomeApp>
```

## Criando container para o projeto

1 - Habilitando todas as permissões para o projeto:

```bash
chmod 777 -R <nomeApp>
```

2 - Acessando o projeto:

```bash
cd <nomeApp>
```

3 - Criando um container:

```bash
docker container run -it --name <nome-container> --privileged --net host -v $PWD:/projeto -v /dev/bus/usb:/dev/bus/usb ygoamaral/react-native:latest bash
```

## Desenvolvimento inicial do app (Android)

_Obs: Os comandos a seguir, são executados dentro do container._

1 - Nas configurações de desenvolvedor em seu dispositivo Android, habilite a "Depuração USB".

2 - Listando os dispositivos conectados (usb):

```bash
adb devices
```

3 - Ao executar o comando acima, aparecerá uma mensagem de autorização no dispositivo. Se você confirmar, execute o comando a seguir para instalar nesse dispositivo:

```bash
yarn android
```

4 - Ignore o erro que possa ter aparecido no dispositivo e execute o comando abaixo:

```bash
yarn start
```

5 - Espere mostrar no terminal a mensagem: "info Dev server ready" e pressione a opção que corresponde a ação "reload app". Geralmente é a tecla "r".

6 - Depois desses passos, já será possível vê a tela "Welcome to React Native" no dispositivo.

## Debugger pelo Google Chrome

1 - Acesse o link: <chrome://inspect>.

2 - Habilite a opção: Discover network targets.

3 - Clique em "Configure".

4 - Adicione um novo alvo: localhost:8081

5 - Clique em "Done"

6 - Na área "Remote Target", clique em "inspect" do alvo "Hermes React Native", para visualizar os logs.

## Gerando instalador Android (apk)

_Obs: Os comandos a seguir, são executados dentro do container._

1 - Entre na pasta do projeto Android

```bash
cd android
```

2 - Gerando o apk

```bash
./gradlew assembleRelease
```

3 - O apk ficará em _android/app/build/outputs/apk/release/app-release.apk_

## Retornando para o projeto

1 - Iniciando o container:

```bash
docker container restart <nome-container>
```

2 - Acessando o projeto:

```bash
docker exec -it <nome-container> bash
```

## Dicas

### Gerando chave de assinatura

Execute dentro do container:

```bash
keytool -genkeypair -v -keystore <name-app>-key.keystore -alias <name-app>-key-alias -keyalg RSA -keysize 2048 -validity 10000
```

### Renomear app

Edite o arquivo _android/app/src/main/res/values/strings.xml_ conforme o exemplo abaixo:

```xml
<string name="app_name">Digite o nome do app aqui</string>
```

### Mudando a versão do app (Android)

No arquivo _\<nomeApp>/android/app/build.gradle_ altere as propriedades _defaultConfig.versionCode_ e _defaultConfig.versionName_.

### Fazendo upgrade da versão do React Native

Acesse o link _<https://react-native-community.github.io/upgrade-helper>_ para verificar quais arquivos deveram ser modificados.

## Tratamento de erros

### Error conflito

Em casos de falha no primeiro _build_ do app, apague todos os containers associados a _ygoamaral/react-native_ para que o conflito possa ser resolvido.

### Error eslintrc

_ESLint: teste/projeto/.eslintrc.js » @react-native-community/eslint-config#overrides[2]: Environment key "jest/globals" is unknown . Please see the 'ESLint' output channel for details._

Solução temporária: apague o arquivo "_.eslintrc.js_".

### Error JAVA_HOME is not set and no 'java' command(...)

Execute dentro do container:

```bash
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre
```

### Falha em requisições http

Poderá ocorrer falhas de requisições http para android 9 ou superior. Adicione as linhas abaixo para fazer o ajuste, no arquivo _nome-projeto/android/app/src/main/AndroidManifest.xml_:

```xml
  <manifest>
    <application
      ...
        android:usesCleartextTraffic="true" // <-- adicione essa linha
      ...>
      ...
    </application>
  </manifest>
```

### Error: ENOSPC: System limit for number of file watchers reached, watch '/projeto/node_modules/

1 - Edite o arquivo: _/etc/sysctl.conf_.

2 - Adicione no final:

```conf
fs.inotify.max_user_watches=524288
```

3 - Aplique a alteração executando o comando, dentro do container:

```bash
sysctl -p
```
